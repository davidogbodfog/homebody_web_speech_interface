#!/bin/bash
# build.sh: homebody web interface installer 
# This script configures apache

INTERFACE_DIR=`pwd`
apache_default_conf=/etc/apache2/sites-enabled/000-default.conf
apache_conf=/etc/apache2/sites-enabled/homebody.conf

function show_usage {
    echo "usage: ./build.sh <option>"
    echo "option can have one of the following values:"
    echo "  localhost (if the system will run locally)"
    echo "  <site_url> (the public website where the server will be hosted)"
}

echo $1
if  [ -z "${1+xxx}" ] || [[ "help -h --h -help --help" =~ $1 ]]; 
then
    show_usage
    exit 1
fi

HOMEBODY_HOST_ADDRESS=$1



### Configure Apache
if [ $HOMEBODY_HOST_ADDRESS = "localhost" ] ;	
then
    echo "Configuring interface to run from localhost"
    sudo cp local.conf ${apache_conf}
    sudo sed -i "s:<interface>:${INTERFACE_DIR}:" ${apache_conf}
else
echo "Configuring interface to run from the public website:" $1

    SSL_DIR=/etc/letsencrypt/live/${HOMEBODY_HOST_ADDRESS}
    if [ ! -d "${SSL_DIR}" ] ;
    then
	echo "ssl key directory ${SSL_DIR} not found."
	echo "make sure you have already used Let's Encrypt to generate your SSL key, and used the same site URL for this script and for Let's Encrypt"
	exit 1	
    fi

    # django configuration + ssl
    sudo cp ssl.conf ${apache_conf}
    sudo sed -i "s:<interface>:${INTERFACE_DIR}:" ${apache_conf}
    sudo sed -i "s:<site>:${HOMEBODY_HOST_ADDRESS}:" ${apache_conf}

    # re-routes http -> https
    sudo cp 000-default.conf ${apache_default_conf}
    sudo sed -i "s:<site>:${HOMEBODY_HOST_ADDRESS}:" ${apache_default_conf}

    # remove conflicting ssl configuration generated by Let's Encrypt
    sudo rm /etc/apache2/sites-enabled/000-default-le-ssl.conf
fi


### Update django files
python manage.py collectstatic
python manage.py makemigrations
python manage.py migrate
chmod 664 db.sqlite3 


### Update js files to reflect the host
if [ $HOMEBODY_HOST_ADDRESS = "localhost" ] ;	
then
    sed -i "s;<site>;ws://localhost:9090;" static/homebody_app/js/main.js
    sed -i "s;<site>;ws://localhost:9090;" static/homebody_app/js/recorder.js
else
    sed -i "s;<site>;wss://${HOMEBODY_HOST_ADDRESS}:9090;" static/homebody_app/js/main.js
    sed -i "s;<site>;wss://${HOMEBODY_HOST_ADDRESS}:9090;" static/homebody_app/js/recorder.js
fi

sudo chgrp -R www-data static/
chmod -R 2750 static/
sudo chown :www-data db.sqlite3 
sudo chown :www-data .
chmod 777 .
