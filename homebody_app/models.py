from __future__ import unicode_literals

from django.db import models
from django.utils import timezone

import datetime

class BackendState(models.Model):
    is_spinning_up = models.BooleanField(False)
    is_running = models.BooleanField(False)
    is_occupied = models.BooleanField(False)
    # user_forwarded_time = models.DateTimeField('User Forward Time')
    user_ticket = models.ForeignKey("Ticket", on_delete = models.SET_NULL, blank=True, null=True)
    dialog_system_interface = models.TextField()

    def __str__(self):
        return "BackendState: is_running: "+str(self.is_running)+", is_occupied: "+str(self.is_occupied)

class MultipleChoiceQuestion(models.Model):
    question_text = models.TextField()
    choice_a = models.CharField(max_length=200)
    choice_b = models.CharField(max_length=200)
    choice_c = models.CharField(max_length=200)
    choice_d = models.CharField(max_length=200)

class MultipleChoiceAnswer(models.Model):
    question = models.ForeignKey("MultipleChoiceQuestion", on_delete = models.CASCADE)
    MULTIPLE_CHOICE_OPTIONS = (
        ("A", 'a'),
        ("B", 'b'),
        ("C", 'c'),
        ("D", 'd'),
    )
    answer = models.CharField(max_length=2, choices=MULTIPLE_CHOICE_OPTIONS)
    ticket = models.ForeignKey("Ticket", on_delete = models.CASCADE)

class FreeTextSurveyQuestion(models.Model):
    question_text = models.TextField()

class FreeTextAnswer(models.Model):
    question = models.ForeignKey("FreeTextSurveyQuestion", on_delete = models.CASCADE)
    answer = models.TextField()
    ticket = models.ForeignKey("Ticket", on_delete = models.CASCADE)

class Ticket(models.Model):
    ticket_id = models.CharField(max_length=200)
    service_state = models.CharField(max_length=200)
    arrival_time = models.DateTimeField('Arrival Time')
    last_ping_time = models.DateTimeField('Last Ping Time')
    nextup = models.BooleanField(False)
    push_to_talk = models.BooleanField(False)
    
    def __str__(self):
        return self.ticket_id

