from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse, Http404
from homebody_app.models import Ticket, BackendState
from django.template import RequestContext, loader
from django.core.urlresolvers import reverse
from django.utils import timezone

import datetime
from random import choice
from string import digits

def index(request):
    try:
        state = BackendState.objects.all()[0]
    except (BackendState.DoesNotExist): 
        # The interface appears to be running independent of the backend. 
        return HttpResponse("Bug I-0003 encountered. Please contact david.cohen@west.cmu.edu with a description of how / what time you reached this page.")
    else:
        if state.dialog_system_interface == "homebody":
            return render(request, 'homebody_app/homebody_landing_page.html')
        elif state.dialog_system_interface == "homebody_app":
            return render(request, 'homebody_app/landing_page.html')
        else:
            raise Http404

def create_ticket(request, ptt):
    ticket = Ticket()
    ticket.ticket_id = ''.join(choice(digits) for i in range(12))
    ticket.service_state = "arrived"
    ticket.arrival_time = timezone.now()
    ticket.last_ping_time = timezone.now()
    ticket.nextup = False
    if ptt == "1":
        ticket.push_to_talk = True
    else:
        ticket.push_to_talk = False
    ticket.save()
    return HttpResponseRedirect(reverse('ticket', args=(ticket.ticket_id,)))


def take_survey(request, ticket_id):
    try:
        ticket = Ticket.objects.get(ticket_id=str(ticket_id))
    except (Ticket.DoesNotExist):
        return HttpResponse("take survey not yet implemented")
        


def ticket_ping(request, ticket_id):
    try:
        state = BackendState.objects.all()[0]
    except (BackendState.DoesNotExist): 
        # The interface appears to be running independent of the backend. 
        return HttpResponse("Bug I-0001 encountered. Please contact david.cohen@west.cmu.edu with a description of how / what time you reached this page.")
    else:

        backend_spinning_up = state.is_spinning_up
        backend_occupied = state.is_occupied
        backend_running = state.is_running

        try:
            ticket = Ticket.objects.get(ticket_id=str(ticket_id))
        except (Ticket.DoesNotExist):
            return HttpResponse("Your ticket has timed out, please return to the landing page and try again")
        else:
            ticket.last_ping_time = timezone.now()
            ticket.save()
            if ticket.nextup and backend_running and not backend_occupied:
                ticket.nextup = False
                ticket.service_state = "in_dialog"
                ticket.save()
                state.is_occupied = True
                state.save()
                if state.dialog_system_interface == "homebody":
                    return render(request, 'homebody_app/homebody.html')
            elif ticket.service_state == "in_dialog":
                ticket.service_state = "dialog_finished"
                ticket.save()
                return HttpResponseRedirect(reverse('done', args=(ticket.ticket_id,)))
            else:
                line_length = Ticket.objects.filter(service_state="arrived").filter(arrival_time__lt=ticket.arrival_time).distinct().count()
                return render(request, 'homebody_app/ticket_ping.html', {'ticket':ticket, 'line_length': line_length, 'backend_spinning_up': backend_spinning_up, 'backend_occupied': backend_occupied, 'backend_running': backend_running})


def done(request, ticket_id):
    return render(request, "homebody_app/thank_you.html")


def status(request):
    try:
        state = BackendState.objects.all()[0]
    except (BackendState.DoesNotExist): 
        # The interface appears to be running independent of the backend. 
        return HttpResponse("Bug I-0002 encountered. Please contact david.cohen@west.cmu.edu with a description of how / what time you reached this page.")

    else:
        backend_spinning_up = state.is_spinning_up
        backend_occupied = state.is_occupied
        backend_running = state.is_running

        line_length = Ticket.objects.filter(service_state="arrived").distinct().count()
        return render(request, 'status.html', {'line_length': line_length, 'backend_spinning_up': backend_spinning_up, 'backend_occupied': backend_occupied, 'backend_running': backend_running})
        
