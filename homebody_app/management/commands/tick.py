from django.core.management.base import BaseCommand, CommandError
from homebody_app.models import Ticket, BackendState
from django.utils import timezone

import datetime
import logging
logger = logging.getLogger(__name__)

TICKET_TIMEOUT = 10          # if a ticket doesn't ping every 10 seconds, they are removed from line

class Command(BaseCommand):
    # args = '<poll_id poll_id ...>'
    help = 'Tick is called every ~ 1 second. It checks the backend availability and demand on the queue. Forwards users to the backend if they are present and the backend is available.'

    def handle(self, *args, **options):
        # get rid of tickets which have timed out
        Ticket.objects.filter(service_state="arrived").filter(last_ping_time__lt=timezone.now()-datetime.timedelta(seconds=TICKET_TIMEOUT)).delete()
        # get oldest ticket, flag it as nextup
        try:
            next_ticket = Ticket.objects.filter(service_state="arrived").order_by('arrival_time')[0]
        except (IndexError):
            logger.debug("No tickets are waiting")
        else:
            next_ticket.nextup = True
            next_ticket.save()
            logger.info("Ticket flagged as next up: "+str(next_ticket.ticket_id))

