from django.core.management.base import BaseCommand, CommandError
from homebody_app.models import BackendState
from django.utils import timezone

import datetime

TICKET_TIMEOUT = 10          # if a ticket doesn't ping every 10 seconds, they are removed from line

class Command(BaseCommand):
    # args = '<poll_id poll_id ...>'
    help = 'Create backend state'


    def add_arguments(self, parser):
        parser.add_argument('-d', required=True, type=str, help="the name of the dialog system interface which we should connect users to.")

    def handle(self, *args, **options):
        BackendState.objects.all().delete()
        dialog_system_interface = options["d"]
        state = BackendState()
        state.is_spinning_up = False
        state.is_running = False
        state.is_occupied = False
        state.dialog_system_interface = dialog_system_interface
        # state.user_ticket = None
        state.save()

