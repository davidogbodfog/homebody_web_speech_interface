from django.core.management.base import BaseCommand, CommandError
from homebody_app.models import Ticket, BackendState
from django.utils import timezone

import datetime

class Command(BaseCommand):
    # args = '<poll_id poll_id ...>'
    help = 'Informs the django interface that the dialog system backend is free'

    def handle(self, *args, **options):
        
        state = BackendState.objects.all()[0]
        state.is_running = False
        state.is_spinning_up = True
        state.is_occupied = False
        # state.user_ticket = None
        state.save()


