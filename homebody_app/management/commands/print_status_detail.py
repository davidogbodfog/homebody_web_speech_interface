from django.core.management.base import BaseCommand, CommandError
from homebody_app.models import Ticket, BackendState
from django.utils import timezone

import datetime
import sys

class Command(BaseCommand):
    # args = '<poll_id poll_id ...>'
    help = 'Print space-separated variables describing the system status'

    def handle(self, *args, **options):
        
        state = BackendState.objects.all()[0]
        sys.stdout.write("is_running "+str(state.is_running)+"\n")
        sys.stdout.write("is_spinning_up "+str(state.is_spinning_up)+"\n")
        sys.stdout.write("is_occupied "+str(state.is_occupied)+"\n")
        sys.stdout.flush()

