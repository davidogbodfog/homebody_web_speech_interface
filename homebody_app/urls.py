from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^status/$', views.status, name='status'),
    url(r'^(?P<ticket_id>\d+)/ticket/$', views.ticket_ping, name='ticket'),
    url(r'^(?P<ticket_id>\d+)/done/$', views.done, name='done'),
    url(r'^(?P<ptt>\d+)/createticket/$', views.create_ticket, name='createticket')
    # url(r'^system/$', views.system_free, name='system')
]
