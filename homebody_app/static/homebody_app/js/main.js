// Connect to the rosbridge server
var connected_to_ros = false
var button_enabled = true

var rbServer = new ROSLIB.Ros({
    url : '<site>'
});

// If there is an error on the backend, an 'error' emit will be emitted.
rbServer.on('error', function(error) {
    console.log(error);
    setTimeout(function(){
	location.reload(true)
    }, 2000);
    
});

// Find out exactly when we made a connection.
rbServer.on('connection', function() {
    connected_to_ros = true
    console.log('Connection made!');
});

//// Push to talk button

var push_to_talk_button_pub = new ROSLIB.Topic({
    ros : rbServer,
    name : '/push_to_talk',
    messageType : 'std_msgs/String'
});

var push_to_talk_listener = new ROSLIB.Topic({
  ros : rbServer,
  name : '/push_to_talk',
  messageType : 'std_msgs/String'
});

push_to_talk_listener.subscribe(function(message) {
    message = message.data
    console.log('Received message: ' + message);
    if (message == "push_confirmed") {
	// Actions to do when push confirmed
	pttButtonState = 1;
	pttUpdateButtonState();
    }
    else if (message == "push_rejected") {
	// Actions to do if push is rejected
	pttButtonState = 2;
	pttUpdateButtonState();
	
    }
    else if (message == "push_completed_vad") {
	pttButtonState = 2;
	pttUpdateButtonState();
    } else {
	console.log("Unknown message: " + message);
    }
});

// 0: button up
// 1: button down
// 2: cooldown
// 3: button disabled (interface is waiting for go-ahead before push is started)
// 4: system is talking
var pttTimeout = 300;
var pttButtonState = 0;
// var buttonTextArr = ['Push to Talk','Speak now', 'Cooldown', 'Please Wait', 'System is Talking'];
var buttonTextArr = ['Talk','Listening...', '', '', ''];
var buttonBgcolorArr = ["#ECCA1D", "#EFFF00", "#CEC388", "#CEC388", "#CEC388"]
var buttonFontColorArr = ["white", "black", "gray", "gray", "gray"]

var disable_push_to_talk = function() {
    $("#pttButton").hide()
    var msg = new ROSLIB.Message({data: "push_disabled"})
    push_to_talk_button_pub.publish(msg)
    button_enabled = false
    pttUpdateButtonState()
}

var pttUpdateButtonState = function() {
    if (button_enabled){
	console.log("updating push-to-talk button: (state = "+pttButtonState+")")
	$("#pttButton").html(buttonTextArr[pttButtonState])
	$("#pttButton").css({'background': buttonBgcolorArr[pttButtonState]})
	$("#pttButton").css({'color': buttonFontColorArr[pttButtonState]})
	
	// Things to do based on button state
	if (pttButtonState == 0){
	    // UP
	    var msg = new ROSLIB.Message({data: "push_lifted"})
	    push_to_talk_button_pub.publish(msg)
	} else if (pttButtonState == 1){
	    // DOWN
	    var msg = new ROSLIB.Message({data: "push_started"})
	    push_to_talk_button_pub.publish(msg)
	} else if (pttButtonState == 2){
	    // cooldown: Disable button
	    window.setTimeout(function() {finishPttCooldown();}, pttTimeout);
	} else if (pttButtonState == 3){
	    // pending push confirmation: Disable button
	} else if (pttButtonState == 4){
	    // system speaking: disable button
	}
    }
}


var onPttButtonClick = function() {
    if (button_enabled){
	// TODO actions to perform on user clicking the PTT button
	if (pttButtonState == 0) {
	    // send Push request
	    var msg = new ROSLIB.Message({data: "push_requested"})
	    push_to_talk_button_pub.publish(msg)
	    // pending push confirmation
	    pttButtonState = 3; 
	    pttUpdateButtonState();
	} else if (pttButtonState == 1) {
	    pttButtonState = 2;
	    pttUpdateButtonState();
	} else {
	    console.log("button clicked, but it is disabled (pttButtonState:"+pttButtonState+")")
	}
    }
};


//// Handle key down
// other js files can register key handlers to keydownFunctionMap

var keydownFunctionMap = {}

// register push-to-talk button
keydownFunctionMap[32] = function(){
    onPttButtonClick()
}

$(document).keydown(function(e){
    console.log("key down detected")
    console.log(e.keyCode)
    if (e.keyCode in keydownFunctionMap){
	console.log("handling key down")
	keydownFunctionMap[e.keyCode]()
    } else {
	console.log("keycode not in handler map")
    }
    // if (e.keyCode == 32){
    // 	onPttButtonClick()
    // }
})

var finishPttCooldown = function() {
    // FInishes cooldown for PTT
    // set button state to up
    console.log("inside finish cooldown, pttUpdateState:"+pttButtonState)
    // if nothing has moved us out of cooldown state, return to lifted state
    if (pttButtonState == 2) {
	pttButtonState = 0;
	pttUpdateButtonState();
    }
}


//// Speak, publish tts state updates
var audioCtx = new (window.AudioContext || window.webkitAudioContext)();
var out_channels = 1;

var tts_status_pub = new ROSLIB.Topic({
    ros : rbServer,
    name : '/tts_state',
    messageType : 'std_msgs/String'
});

var tts_audio_listener = new ROSLIB.Topic({
    ros : rbServer,
    name : '/tts_audio_stream',
    messageType : 'audio_common_msgs/AudioData'
});

function stringToUint(string) {
    var string = atob(unescape(encodeURIComponent(string))),
    charList = string.split(''),
    uintArray = [];
    for (var i = 0; i < charList.length; i++) {
    	uintArray.push(charList[i].charCodeAt(0));
    }
    return new Uint8Array(uintArray);
}

tts_audio_listener.subscribe(function(message){
    u8 = stringToUint(message.data)
    var i16 = new Int16Array(u8.buffer);
    var float32_array = new Float32Array(i16, 0, i16.length*Int16Array.BYTES_PER_ELEMENT)
    var ttsArrayBuffer = audioCtx.createBuffer(out_channels, float32_array.length, 16000);
    var nowBuffering = ttsArrayBuffer.getChannelData(0);
    for (var i = 0; i < float32_array.length; i++) {
    	nowBuffering[i] = (float32_array[i]/32768);
    }
    var out_source = audioCtx.createBufferSource()
    out_source.buffer = ttsArrayBuffer;
    //console.log(out_source.buffer.getChannelData(0))
    out_source.connect(audioCtx.destination);
    console.log("publishing tts_start")
    var msg = new ROSLIB.Message({data: "tts_start"})
    tts_status_pub.publish(msg)
    pttButtonState = 4
    pttUpdateButtonState()
    out_source.start();
    // use setTimeout to ensure tts finish method is returned
    setTimeout(function(){
	console.log("publishing tts_finish")
	var msg = new ROSLIB.Message({data: "tts_finish"})
	tts_status_pub.publish(msg)
	pttButtonState = 2
	pttUpdateButtonState()
    }, ttsArrayBuffer.length * 1.0 / 16)
});


//// Refresh the page (and get redirected) whenever the dialog ends

var dialog_command_listener = new ROSLIB.Topic({
  ros : rbServer,
  name : '/dialog_control',
  messageType : 'std_msgs/String'
});

dialog_command_listener.subscribe(function(message) {
    if (["successful_completion", "system_crash", "system_give_up", "user_timeout", "system_crash", "system_give_up", "user_timeout", "user_never_arrived",'"successful_completion"', '"system_crash"', '"system_give_up"', '"user_timeout"', '"system_crash"', '"system_give_up"', '"user_timeout"', '"user_never_arrived"'].indexOf(message.data) >=0) {
	console.log("received dialog command:"+ message)
	window.setTimeout(function() {
	    location.reload(true)
	}, 5000)
    } else if (message.data == "dm_started"){
	disable_push_to_talk()
    }
})


