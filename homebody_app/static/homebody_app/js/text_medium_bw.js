// Connecting to ROS
// -----------------
var ros_medium_bw = new ROSLIB.Ros();

// If there is an error on the backend, an 'error' emit will be emitted.
ros_medium_bw.on('error', function(error) {
  console.log(error);
});

// Find out exactly when we made a connection.
ros_medium_bw.on('connection', function() {
  console.log('Connection made!');
});

// Create a connection to the rosbridge WebSocket server.
ros_medium_bw.connect('ws://localhost:9090');

//Subscribing to a Topic
//----------------------
var listener_medium_bw = new ROSLIB.Topic({
  ros : ros_medium_bw,
  name : '/speech/hypothesis/best_bw_medium',
  messageType : 'std_msgs/String'
});
var temp_str_medium_bw ="";
listener_medium_bw.subscribe(function(message) {	
  
  for(var c_medium_bw=0; c_medium_bw<message.data.length; c_medium_bw++)
  {
    var elem_medium_bw = document.getElementById("speech_medium_bw");
    temp_str_medium_bw+=message.data[c_medium_bw];
    if(message.data[c_medium_bw] == ' ')
    {	
	elem_medium_bw.value = temp_str_medium_bw;
	//console.log('Received message on ' + listener.name + ': '+ temp_str);
    }
    else if(message.data[c_medium_bw] == '\n')
    {
          temp_str_medium_bw = "";
    }
  }
});

