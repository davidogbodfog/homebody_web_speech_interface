var running = false;  
var break_pressure_array;
var engine_rpm_array;  
var speed_array;
var steering_array;
var window_aperture_0_array; 
var window_aperture_1_array; 
var window_aperture_2_array; 
var window_aperture_3_array; 
var yaw_rate_array;
var heart_rate_array;
var respiration_rate_array;
var skin_temperature_array;
var posture_array;
var peak_acceleration_array;
var epoc_f3_array;
var epoc_fc6_array;
var epoc_p7_array;
var epoc_t8_array;
var epoc_f7_array;
var epoc_f8_array;
var epoc_t7_array;
var epoc_p8_array;
var epoc_af4_array;
var epoc_f4_array;
var epoc_af3_array;
var epoc_02_array;
var epoc_01_array;
var epoc_fc5_array;
var epoc_x_array;
var epoc_y_array;
var xscale = 100;   
var break_pressure = 0.0;
var engine_rpm = 0.0;
var speed = 0.0;
var steering = 0.0;
var window_aperture_0 = 0.0; 
var window_aperture_1 = 0.0; 
var window_aperture_2 = 0.0; 
var window_aperture_3 = 0.0; 
var yaw_rate = 0.0;
var heart_rate = 0.0;
var respiration_rate = 0.0;
var skin_temperature = 0.0;
var posture = 0.0;
var peak_acceleration = 0.0;
var epoc_f3 = 0.0;
var epoc_fc6 = 0.0;
var epoc_p7 = 0.0;
var epoc_t8 = 0.0;
var epoc_f7 = 0.0;
var epoc_f8 = 0.0;
var epoc_t7 = 0.0;
var epoc_p8 = 0.0;
var epoc_af4 = 0.0;
var epoc_f4 = 0.0;
var epoc_af3 = 0.0;
var epoc_02 = 0.0;
var epoc_01 = 0.0;
var epoc_fc5 = 0.0;
var epoc_x = 0.0;
var epoc_y = 0.0;
var stack = 0
 
  // Connecting to ROS
  // -----------------
  var ros = new ROSLIB.Ros();

  // If there is an error on the backend, an 'error' emit will be emitted.
  ros.on('error', function(error) {
    console.log(error);
  });

  // Find out exactly when we made a connection.
  ros.on('connection', function() {
    console.log('Connection made!');
  });

  // Create a connection to the rosbridge WebSocket server.
  ros.connect('ws://localhost:9090');

  //Subscribing to a Topic
  //----------------------
  var listener = new ROSLIB.Topic({
    ros : ros,
    name : '/can_data',
    messageType : 'std_msgs/String'
  });

  var listener_chatter = new ROSLIB.Topic({
    ros : ros,
    name : '/chatter2',
    messageType : 'bar_msgs/GP'
  });
  
  var listener_epoc = new ROSLIB.Topic({
    ros : ros,
    name : '/epoc/frames',
    messageType : 'emotiv_epoc/EEGFrame'
  });

  listener_chatter.subscribe(function(message) {	
    console.log('Received message on ' + listener_chatter.name + ': '+ message.heart_rate);
    heart_rate = message.heart_rate; 
    respiration_rate = message.respiration_rate;
    skin_temperature = message.skin_temperature;
    posture = message.posture;
    peak_acceleration = message.peak_acceleration;
  });

  listener_epoc.subscribe(function(message) {	
    //console.log('Received message on ' + listener_epoc.name + ': '+ message.channel_names[0]);
    epoc_f3 = message.signals[0]; 
    epoc_fc6 = message.signals[1]; 
    epoc_p7 = message.signals[2]; 
    epoc_t8 = message.signals[3]; 
    epoc_f7 = message.signals[4]; 
    epoc_f8 = message.signals[5]; 
    epoc_t7 = message.signals[6]; 
    epoc_p8 = message.signals[7]; 
    epoc_af4 = message.signals[8]; 
    epoc_f4 = message.signals[9]; 
    epoc_af3 = message.signals[10]; 
    epoc_02 = message.signals[11]; 
    epoc_01 = message.signals[12]; 
    epoc_fc5 = message.signals[13]; 
    epoc_x = message.signals[14]; 
    epoc_y = message.signals[15]; 
  });

  listener.setMaxListeners(0)

  listener.subscribe(function(message) {	
    //document.getElementById("myspan").innerHTML=message.orientation_covariance[0];
    //console.log('Received message on ' + listener.name + ': '+ message.data);
    var temp_str ="";
    var float_array={};
    var j=0;
    for(var c=0; c<message.data.length; c++)
    {
        if(message.data[c] != " ")
	    temp_str+=message.data[c];
        else
        {
	    float_array[j] = parseFloat(temp_str);
	    temp_str="";
	    j++;
        }
    }
    break_pressure = float_array[0];
    engine_rpm = float_array[1]; 
    speed = float_array[4];
    steering = float_array[5];
    window_aperture_0 = float_array[6]; 
    window_aperture_1 = float_array[7]; 
    window_aperture_2 = float_array[8]; 
    window_aperture_3 = float_array[8]; 
    yaw_rate = float_array[10];
    });    

//this function does the plotting   
function draw() {  

   //shift the values to left  
    for (var i = 0; i < this.xscale - 1; i++) {  
        this.break_pressure_array[i] = [i,this.break_pressure_array[i + 1][1]]; 
	this.engine_rpm_array[i] = [i,this.engine_rpm_array[i + 1][1]]; // (x,y)  
	this.speed_array[i] = [i,this.speed_array[i + 1][1]];
	this.steering_array[i] = [i,this.steering_array[i + 1][1]];
	this.window_aperture_0_array[i] = [i,this.window_aperture_0_array[i + 1][1]]; 
	this.window_aperture_1_array[i] = [i,this.window_aperture_1_array[i + 1][1]]; 
	this.window_aperture_2_array[i] = [i,this.window_aperture_2_array[i + 1][1]]; 
	this.window_aperture_3_array[i] = [i,this.window_aperture_3_array[i + 1][1]]; 
	this.yaw_rate_array[i] = [i,this.yaw_rate_array[i + 1][1]];
	this.heart_rate_array[i] = [i,this.heart_rate_array[i + 1][1]];
	this.respiration_rate_array[i] = [i,this.respiration_rate_array[i + 1][1]];
	this.skin_temperature_array[i] = [i,this.skin_temperature_array[i + 1][1]];
	this.posture_array[i] = [i,this.posture_array[i + 1][1]];
	this.peak_acceleration_array[i] = [i,this.peak_acceleration_array[i + 1][1]];
	this.epoc_f3_array[i] = [i,this.epoc_f3_array[i + 1][1]];

	this.epoc_fc6_array[i] = [i,this.epoc_fc6_array[i + 1][1]];
	this.epoc_p7_array[i] = [i,this.epoc_p7_array[i + 1][1]];
	this.epoc_t8_array[i] = [i,this.epoc_t8_array[i + 1][1]];
	this.epoc_f7_array[i] = [i,this.epoc_f7_array[i + 1][1]];
	this.epoc_f8_array[i] = [i,this.epoc_f8_array[i + 1][1]];
	this.epoc_t7_array[i] = [i,this.epoc_t7_array[i + 1][1]];
	this.epoc_p8_array[i] = [i,this.epoc_p8_array[i + 1][1]];
	this.epoc_af4_array[i] = [i,this.epoc_af4_array[i + 1][1]];
	this.epoc_f4_array[i] = [i,this.epoc_f4_array[i + 1][1]];
	this.epoc_af3_array[i] = [i,this.epoc_af3_array[i + 1][1]];
	this.epoc_02_array[i] = [i,this.epoc_02_array[i + 1][1]];
	this.epoc_01_array[i] = [i,this.epoc_01_array[i + 1][1]];
	this.epoc_fc5_array[i] = [i,this.epoc_fc5_array[i + 1][1]];
	this.epoc_x_array[i] = [i,this.epoc_x_array[i + 1][1]];
	this.epoc_y_array[i] = [i,this.epoc_y_array[i + 1][1]];

    }  

    //add the new coming value to the last postion  
    this.break_pressure_array[this.xscale - 1] = [this.xscale - 1, break_pressure];
    this.engine_rpm_array[this.xscale - 1] = [this.xscale - 1, engine_rpm];  
    this.speed_array[this.xscale - 1] = [this.xscale - 1, speed];
    this.steering_array[this.xscale - 1] = [this.xscale - 1, steering];
    this.window_aperture_0_array[this.xscale - 1] = [this.xscale - 1, window_aperture_0];
    this.window_aperture_1_array[this.xscale - 1] = [this.xscale - 1, window_aperture_1];
    this.window_aperture_2_array[this.xscale - 1] = [this.xscale - 1, window_aperture_2];
    this.window_aperture_3_array[this.xscale - 1] = [this.xscale - 1, window_aperture_3];
    this.yaw_rate_array[this.xscale - 1] = [this.xscale - 1, yaw_rate];
    this.heart_rate_array[this.xscale - 1] = [this.xscale - 1, heart_rate];
    this.respiration_rate_array[this.xscale - 1] = [this.xscale - 1, respiration_rate];
    this.skin_temperature_array[this.xscale - 1] = [this.xscale - 1, skin_temperature];
    this.posture_array[this.xscale - 1] = [this.xscale - 1, posture];
    this.peak_acceleration_array[this.xscale - 1] = [this.xscale - 1, peak_acceleration];
    this.epoc_f3_array[this.xscale - 1] = [this.xscale - 1, epoc_f3];
    this.epoc_fc6_array[this.xscale - 1] = [this.xscale - 1, epoc_fc6];
    this.epoc_p7_array[this.xscale - 1] = [this.xscale - 1, epoc_p7];
    this.epoc_t8_array[this.xscale - 1] = [this.xscale - 1, epoc_t8];
    this.epoc_f7_array[this.xscale - 1] = [this.xscale - 1, epoc_f7];
    this.epoc_f8_array[this.xscale - 1] = [this.xscale - 1, epoc_f8];
    this.epoc_t7_array[this.xscale - 1] = [this.xscale - 1, epoc_t7];
    this.epoc_p8_array[this.xscale - 1] = [this.xscale - 1, epoc_p8];
    this.epoc_af4_array[this.xscale - 1] = [this.xscale - 1, epoc_af4];
    this.epoc_f4_array[this.xscale - 1] = [this.xscale - 1, epoc_f4];
    this.epoc_af3_array[this.xscale - 1] = [this.xscale - 1, epoc_af3];
    this.epoc_02_array[this.xscale - 1] = [this.xscale - 1, epoc_02];
    this.epoc_01_array[this.xscale - 1] = [this.xscale - 1, epoc_01];
    this.epoc_fc5_array[this.xscale - 1] = [this.xscale - 1, epoc_fc5];
    this.epoc_x_array[this.xscale - 1] = [this.xscale - 1, epoc_x];
    this.epoc_y_array[this.xscale - 1] = [this.xscale - 1, epoc_y];


        $.plot($("#breakpressure"), [  
            {   
                data: this.break_pressure_array,  
                lines: { show: true, lineWidth: 1.5 },
                	color:"#a7a"  
            }  
        ],  
        {
	  xaxis: {
	    show: false
	  }
	});
	$.plot($("#enginerpm"), [  
            {   
                data: this.engine_rpm_array,  
                lines: { show: true, lineWidth: 1.5 },
                	color:"#bf3"  
            }  
        ],  
        {	
	  xaxis: {
	    show: false
	  }
	});  
	$.plot($("#speed"), [  
	    {   
		data: this.speed_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"#c5f",
		  
	    }  
		
	],  
	{
	  xaxis: {
	    show: false
	  }
	});
	$.plot($("#steering"), [  
	    {   
		data: this.steering_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"#a46"  
	    }  
	],  
	{
	  xaxis: {
	    show: false
	  }
	});
	$.plot($("#windowaperture0"), [  
	    {   
		data: this.window_aperture_0_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"blue"  
	    }  
	],  
	{
	  xaxis: {
	    show: false
	  }
	});
	$.plot($("#windowaperture1"), [  
	    {   
		data: this.window_aperture_1_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"cyan"  
	    }  
	],  
	{
	  xaxis: {
	    show: false
	  }
	});
	$.plot($("#windowaperture2"), [  
	    {   
		data: this.window_aperture_2_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"yellow"  
	    }  
	],  
	{
	  xaxis: {
	    show: false
	  }
	});
	$.plot($("#windowaperture3"), [  
	    {   
		data: this.window_aperture_3_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"red"  
	    }  
	],  
	{
	  xaxis: {
	    show: false
	  }
	});
	$.plot($("#yawrate"), [  
	    {   
		data: this.yaw_rate_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"green"  
	    }  
	],  
	{
	  xaxis: {
	    show: false
	  }
	});
	$.plot($("#heartrate"), [  
	    {   
		data: this.heart_rate_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"green"  
	    }  
	],  
	{
	  xaxis: {
	    show: false
	  }
	});
	$.plot($("#respirationrate"), [  
	    {   
		data: this.respiration_rate_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"yellow"  
	    }  
	],  
	{
 	  xaxis: {
	    show: false
	  }
	});
	$.plot($("#skintemperature"), [  
	    {   
		data: this.skin_temperature_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"red"  
	    }  
	],  
	{
  	  xaxis: {
	    show: false
	  }
	});
	$.plot($("#posture"), [  
	    {   
		data: this.posture_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"blue"  
	    }  
	],  
	{
	  xaxis: {
	    show: false
	  }
	});
	$.plot($("#peakacceleration"), [  
	    {   
		data: this.peak_acceleration_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"black"  
	    }  
	],  
	{
	  xaxis: {
	    show: false
	  }
	});


	$.plot($("#eegdata"), [  
	    {   
		data: this.epoc_f3_array, 
		lines: { show: true, lineWidth: 1.5 },
			color:"#FE6669"  
	    },
	    {   
		data: this.epoc_fc6_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"#8E12CC"  
	    },
	    {   
		data: this.epoc_p7_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"#6A47B5"  
	    },
	    {   
		data: this.epoc_t8_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"#477AB5"  
	    },
	    {   
		data: this.epoc_f7_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"#47B5A3"  
	    }, 
	    {   
		data: this.epoc_f8_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"#47B553"  
	    },
	    {   
		data: this.epoc_t7_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"#9DB547"  
	    },
	    {   
		data: this.epoc_p8_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"#B5AE47"  
	    }, 
	    {   
		data: this.epoc_af4_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"#B58E47"  
	    },
	    {   
		data: this.epoc_f4_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"#B54847"  
	    },
	    {   
		data: this.epoc_af3_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"#AD9B9A"  
	    },
	    {   
		data: this.epoc_02_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"#B3A47B"  
	    },
	    {   
		data: this.epoc_01_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"#7BAFB3"  
	    },
	    {   
		data: this.epoc_fc5_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"#A37BB3"  
	    },
	    {   
		data: this.epoc_x_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"#B37B8F"  
	    },
	    {   
		data: this.epoc_y_array,  
		lines: { show: true, lineWidth: 1.5 },
			color:"#7BB385"  
	    }  
	],  
	{
	  xaxis: {
	    show: false
	  },
	  series: {
	    stack: stack
	  },
          yaxis: {
            //show:false 
	    min:0, max: 140000,  tickSize: 10000 
          }
	});
}  

//This creates the data array with 0.0 inital Y values at the initialization time  
function initialize() {  
    this.break_pressure_array = new Array();  
    this.engine_rpm_array = new Array();
    this.speed_array = new Array();
    this.steering_array = new Array();
    this.window_aperture_0_array = new Array();
    this.window_aperture_1_array = new Array();
    this.window_aperture_2_array = new Array();
    this.window_aperture_3_array = new Array();
    this.yaw_rate_array = new Array();
    this.heart_rate_array = new Array();
    this.respiration_rate_array = new Array();
    this.skin_temperature_array = new Array();
    this.posture_array = new Array();
    this.peak_acceleration_array = new Array();
    this.epoc_f3_array = new Array();
    this.epoc_fc6_array = new Array();
    this.epoc_p7_array = new Array();
    this.epoc_t8_array = new Array();
    this.epoc_f7_array = new Array();
    this.epoc_f8_array = new Array();
    this.epoc_t7_array = new Array();
    this.epoc_p8_array = new Array();
    this.epoc_af4_array = new Array();
    this.epoc_f4_array = new Array();
    this.epoc_af3_array = new Array();
    this.epoc_02_array = new Array();
    this.epoc_01_array = new Array();
    this.epoc_fc5_array = new Array();
    this.epoc_x_array = new Array();
    this.epoc_y_array = new Array();

    for (var i = 0; i < xscale; i++) {  
        this.break_pressure_array[i] = [i, 0.0];  
	this.engine_rpm_array[i] = [i, 0.0];
	this.speed_array[i] = [i, 0.0];
	this.steering_array[i] = [i, 0.0];
	this.window_aperture_0_array[i] = [i, 0.0]; 
	this.window_aperture_1_array[i] = [i, 0.0]; 
	this.window_aperture_2_array[i] = [i, 0.0]; 
	this.window_aperture_3_array[i] = [i, 0.0]; 
	this.yaw_rate_array[i] = [i, 0.0];
	this.heart_rate_array[i] = [i, 0.0];
	this.respiration_rate_array[i] = [i, 0.0];
	this.skin_temperature_array[i] = [i, 0.0];
	this.posture_array[i] = [i, 0.0];
	this.peak_acceleration_array[i] = [i, 0.0];
	this.epoc_f3_array[i] = [i, 0.0];
	this.epoc_f3_array[i] = [i, 0.0];
	this.epoc_fc6_array[i] = [i, 0.0];
	this.epoc_p7_array[i] = [i, 0.0];
	this.epoc_t8_array[i] = [i, 0.0];
	this.epoc_f7_array[i] = [i, 0.0];
	this.epoc_f8_array[i] = [i, 0.0];
	this.epoc_t7_array[i] = [i, 0.0];
	this.epoc_p8_array[i] = [i, 0.0];
	this.epoc_af4_array[i] = [i, 0.0];
	this.epoc_f4_array[i] = [i, 0.0];
	this.epoc_af3_array[i] = [i, 0.0];
	this.epoc_02_array[i] = [i, 0.0];
	this.epoc_01_array[i] = [i, 0.0];
	this.epoc_fc5_array[i] = [i, 0.0];
	this.epoc_x_array[i] = [i, 0.0];
	this.epoc_y_array[i] = [i, 0.0];
    }  
}  

//This is used to generate ticks for X axis (value 0 will be on the right)  
function generateTicks() {  
    var tickArray = [];  
    var startTick = 20;  
    var i = startTick - 1;  
    var weight = this.xscale / 20;  
    do {  
        var t = (startTick - i) * weight - 1;  
        var v = i * weight;  
        if (v == 0) {  
            v = "0";  
        }  
        tickArray.push([t, v]);  
        i--;  
    } while (i > -1);  
    return tickArray;  
}  

//This function is called once per every 1000ms  
function refreshStat() {  

    if (!running) {  
        running = true;  
        draw();  
        running = false;  
    } 
}  

$(document).ready(function () {  
	initialize();  
	refreshStat();  
	setInterval("refreshStat()", 1000);  
});
