var map;
var marker;
var lat = 37.410;
var lng = -122.0598;
var bermudaTriangle = new google.maps.Polygon();

// Connecting to ROS
// -----------------
var ros = new ROSLIB.Ros();

// If there is an error on the backend, an 'error' emit will be emitted.
ros.on('error', function(error) {
  console.log(error);
});

// Find out exactly when we made a connection.
ros.on('connection', function() {
  console.log('Connection made!');
});

// Create a connection to the rosbridge WebSocket server.
ros.connect('ws://localhost:9090');

//Subscribing to a Topic
//----------------------
var listener = new ROSLIB.Topic({
  ros : ros,
  name : '/imu/gps/fix',
  messageType : 'sensor_msgs/NavSatFix'
});
var listener_gaze = new ROSLIB.Topic({
  ros : ros,
  name : '/dnn_output',
  messageType : 'std_msgs/Int8'
});

listener.subscribe(function(message) {	

  console.log('Received message on ' + listener_gaze.name + ': '+ message.longitude);
  marker.setPosition(new google.maps.LatLng(message.latitude, message.longitude));

  lat = message.latitude;
  lng = message.longitude;

});


listener_gaze.subscribe(function(message) {	
  var gaze_offset;
  console.log('Received message on ' + listener_gaze.name + ': '+ message.data);
  bermudaTriangle.setMap(null);
  
  switch(message.data)
  {
    case 0: gaze_offset=135; gaze_info='Driver window';gaze_color="#d81b21";break;
    case 1: gaze_offset=108; gaze_info='Left of center';gaze_color="#da5867";break;
    case 2: gaze_offset=  81; gaze_info='straight ahead';gaze_color="#f47c20";break;
    case 3: gaze_offset=54; gaze_info='Right of center';gaze_color="#538018";break;
    case 4: gaze_offset=27; gaze_info='Passenger window';gaze_color="#0095cd";break;
    default: gaze_offset= 81; gaze_info='';break;
  }

  var triangleCoords = [
    new google.maps.LatLng(lat, lng),
    new google.maps.LatLng(lat+(0.06*Math.cos((gaze_offset+22.5)*0.0174532925)/111),lng+(0.06*Math.sin((gaze_offset+22.5)*0.0174532925)/88)),
    new google.maps.LatLng(lat+(0.06*Math.cos((gaze_offset-22.5)*0.0174532925)/111),lng+(0.06*Math.sin((gaze_offset-22.5)*0.0174532925)/88)),
    new google.maps.LatLng(lat, lng)
  ];

  var polyOptions = {
    paths: triangleCoords,
    strokeColor: '#FF0000',
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: '#FF0000',
    fillOpacity: 0.35
  }

  bermudaTriangle .setOptions(polyOptions);
  bermudaTriangle.setMap(map);

  //map.panTo( new google.maps.LatLng(lat, lng));

});

function init() {
  var map_canvas = document.getElementById('map_canvas');
  var map_options = {
    center: new google.maps.LatLng(lat, lng),
    zoom: 18,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  map = new google.maps.Map(map_canvas, map_options);

  var latLng = new google.maps.LatLng(lat,lng);
  marker = new google.maps.Marker({
    position: latLng,
    map: map,
    icon: "img/marker.png"
  });	
}
google.maps.event.addDomListener(window, 'load', init);



