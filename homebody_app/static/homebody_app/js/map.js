var gaze_offset=0;
var temp=0;
var map;
var sv;
var bermudaTriangle;
var triangleCoords;
var line;
var panorama;
var a=0;
var restaurants = [
        ['Sino', 37.4088083, -122.05892, 1],
        ['Left Bank', 37.4085, -122.0590, 2],
        ['Vintage Wine Bar', 37.408227, -122.05907, 3],
        ['Consuelo', 37.407956, -122.0590861, 4],
        ['Thea Mediterranean Cuisine', 37.4077361, -122.05916, 5],
        ['Pasta Pomodoro', 37.4083805, -122.05941, 6],
        ['Yankee Pier', 37.4081, -122.059514, 7],
        ['Blowfish Sushi', 37.4078861, -122.05958, 8],
        ['Yuki Sushi', 37.407494, -122.05971, 9],
        ['Hoshi', 37.40725, -122.05978, 10],
        ['Dia De Pesa', 37.407306, -122.0588, 11],
        ['Tacos Al Pastor', 37.407025,-122.0589027, 12],
        ['Rubios', 37.4067528, -122.0589972, 13],
        ['China Garden', 37.40677, -122.05943, 14],
        ['Gyros House', 37.4088083, -122.05892, 15],
        ['Hong Kong Bistro', 37.394405, -122.078445, 16],
        ['King of Krung Siam', 37.394028, -122.079056, 17],
        ['Kitchen Table', 37.394592, -122.078629, 18],
        ['Nami Nami', 37.393379, -122.079643, 19],
        ['Pasta Q', 37.394436, -122.078926, 20],
        ['Pho Hoa', 37.393661, -122.079445, 21],
        ['Queens House', 37.392944, -122.079361, 22],
        ['Sakoon', 37.3916555, -122.0801166, 23],
        ['Scratch', 37.3908777, -122.0805361, 24],
        ['Shabuway', 37.394199, -122.078903, 25],
        ['Sono Sushi', 37.3917583, -122.0800556, 26],
        ['Temptations', 37.3928305, -122.079775, 27],
        ['Xanh', 37.3948388, -122.0785194, 28],
 //       ['Zucca Ristorante', 37.394161, -122.079102, 29],
        ['Chef Liu', 37.3934417, -122.0795083, 30],
        ['Ephesus', 37.3939472, -122.07865, 31],
        ['Redrock', 37.3935861, -122.0789638, 32],
        ['Amarin Thai', 37.3942639, -122.0789972, 33],
];

function setMarkers(map, locations) {
        // Add markers to the map
        var image = new google.maps.MarkerImage('images/Restaurant-Blue-icon.png',
        new google.maps.Size(30, 30),
        new google.maps.Point(0,0),
        new google.maps.Point(20, 20));
        var shape = {
         coord: [1, 1, 1, 20, 18, 20, 18 , 1],
         type: 'poly'
        };
        for (var i = 0; i < locations.length; i++) {
        var restaurant = locations[i];
        var myLatLng = new google.maps.LatLng(restaurant[1], restaurant[2]);
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: image,
            shape: shape,
            title: restaurant[0],
            zIndex: restaurant[3]
        });
	}
}

function processSVData(data, status) {
        if (status == google.maps.StreetViewStatus.OK) {
          panorama.setPano(data.location.pano);
          panorama.setPov({
            heading: a,
            pitch: 0,
            zoom: 1
          });
          panorama.setVisible(true);
        } else {
          alert('Street View data not found for this location.');
        }
}

function newmap() {
	  		
	var connection = new ros.Connection("ws://localhost:9090");
	connection.setOnClose(function (e) {
          document.write('connection closed <br/>');
      	});
     	connection.setOnError(function (e) {
          document.write('error....<br/>');
      	});
	connection.setOnOpen(function (e) {
        connection.addHandler('/extended_fix',function(msg) {
	  y= msg.latitude;
	  x= msg.longitude;
          a= msg.track;
	  var carLatLng=new google.maps.LatLng(y,x);
	  map.setCenter(carLatLng);
	  sv.getPanoramaByLocation(carLatLng, 50, processSVData);

//gaze triangle refresh
          triangleCoords = [
            carLatLng,
            new google.maps.LatLng(y+(0.06*Math.cos((a+gaze_offset+22.5)*0.0174532925)/111),x+(0.06*Math.sin((a+gaze_offset+22.5)*0.0174532925)/88)),
            new google.maps.LatLng(y+(0.06*Math.cos((a+gaze_offset-22.5)*0.0174532925)/111),x+(0.06*Math.sin((a+gaze_offset-22.5)*0.0174532925)/88)),
            carLatLng
          ];

 
          bermudaTriangle.setOptions({
            paths: triangleCoords,
            strokeColor: "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#FF0000",
            fillOpacity: 0.5
          });

//car arrow refresh
          var lineCoordinates = [
          carLatLng,
	  new google.maps.LatLng(y+(0.0001*Math.cos(a*0.0174532925)/111),x+(0.0001*Math.sin(a*0.0174532925)/88))
          ];
          var lineSymbol = {
            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
          };
          line.setOptions({
            path: lineCoordinates,
            icons: [{
            icon: lineSymbol,
            offset: '100%'
            }],
            map: map
          });
        });
	  connection.callService('/rosjs/subscribe','["/extended_fix",300]',function(rsp) {});     
        });
}
function init_map(){
	  var mapOptions = {
          	zoom: 18,
                mapTypeId: google.maps.MapTypeId.SATELLITE
       	  };
var fenway = new google.maps.LatLng(42.345573,-71.098326);
var panoOptions = {
  addressControlOptions: {
    position: google.maps.ControlPosition.LEFT_TOP
  },
  visible:true
};
          map = new google.maps.Map(document.getElementById("map_canvas"),mapOptions);
	sv = new google.maps.StreetViewService();

        panorama = new google.maps.StreetViewPanorama(document.getElementById('pano'),panoOptions);
//restaurant	
	  setMarkers(map, restaurants);

//gaze triangle init
          triangleCoords = [
            new google.maps.LatLng(37.4088083, -122.05892),
            new google.maps.LatLng(37.4088083, -122.05892),
            new google.maps.LatLng(37.4088083, -122.05892),
          ];

          bermudaTriangle=new google.maps.Polygon({
            paths: triangleCoords,
            strokeColor: "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#FF0000",
            fillOpacity: 0.5
          });
          bermudaTriangle.setMap(map);

//car arrow!!
          var lineCoordinates = [
          new google.maps.LatLng(37.4088083, -122.05892),
            new google.maps.LatLng(37.4088083, -122.05892)
          ];
          var lineSymbol = {
            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
          };
          line = new google.maps.Polyline({
            path: lineCoordinates,
            icons: [{
            icon: lineSymbol,
            offset: '100%'
            }],
            map: map
          });


}
function getgaze() {
	init_map();
	var connection2 = new ros.Connection("ws://localhost:9090");
	connection2.setOnClose(function (e) {
          document.write('connection closed <br/>');
      	});
     	connection2.setOnError(function (e) {
          document.write('error....<br/>');
      	});
	connection2.setOnOpen(function (e) {
          connection2.addHandler('/driver_gaze',function(msg) {
            zone= msg.value;
	    switch(zone)
	    {
		case 1: gaze_offset=-90; gaze_info='Driver window';break;
		case 2: gaze_offset=-45; gaze_info='Left of center';break;
		case 3: gaze_offset=  0; gaze_info='straight ahead';break;
		case 4: gaze_offset= 45; gaze_info='Right of center';break;
		case 5: gaze_offset= 90; gaze_info='Passenger window';break;
		default: gaze_offset= 0; gaze_info='';break;
	    }
	    document.getElementById('gazeinfo').innerHTML ='Driver\'s gaze: '+gaze_info;
	  });
	  connection2.callService('/rosjs/subscribe','["/driver_gaze",200]',function(rsp) {});     
        });
}


