/*License (MIT)

Copyright © 2013 Matt Diamond

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE.
*/

(function(window){
    
    // This function connects to the rosbridge server running on the local computer on port 9090
    var rbServer = new ROSLIB.Ros({
    	url : '<site>'
    });
    
    
    // If there is an error on the backend, an 'error' emit will be emitted.
    rbServer.on('error', function(error) {
  	console.log(error);
    });
    
    // Find out exactly when we made a connection.
    rbServer.on('connection', function() {
  	console.log('Connection made!');
    });
    
    
    var WORKER_PATH = 'js/recorderjs/recorderWorker.js';
    
    var Recorder = function(source, cfg){
	var config = cfg || {};
	var bufferLen = config.bufferLen || 1024;
	this.context = source.context;
	if(!this.context.createScriptProcessor){
	    this.node = this.context.createJavaScriptNode(bufferLen, 2, 2);
	} else {
	    this.node = this.context.createScriptProcessor(bufferLen, 2, 2);
	}
	
	
	var worker = new Worker(config.workerPath || WORKER_PATH);
	console.log(this.context.sampleRate);
	worker.postMessage({
	    command: 'init',
	    config: {
		sampleRate: this.context.sampleRate
	    }
	});
	
	
	
	//ROS STUFF
	var dialog_ctrl_pub = new ROSLIB.Topic({
  	    ros : rbServer,
  	    name : '/dialog_control',
  	    messageType : 'std_msgs/String'
	});
	
	//ROS STUFF
	var audio_pub = new ROSLIB.Topic({
  	    ros : rbServer,
  	    name : '/audio',
  	    messageType : 'audio_common_msgs/AudioData'
	});
	
	var dialog_ctrl_msg = new ROSLIB.Message({data: "user_arrive"});
        dialog_ctrl_pub.publish(dialog_ctrl_msg);
	
	
	function swap16(val) {
    	    return ((val & 0xFF) << 8)
		| ((val >> 8) & 0xFF);
	}
	
	function arrayByteToInt16(byteArray) {
	    //var ints = [];
	    var ints = new Int16Array(byteArray.length/2)
	    for (var i = 0; i < byteArray.length; i += 2) {
		// Note: assuming Little-Endian here
		ints[i/2] = ((byteArray[i+1] <<8) | (byteArray[i] ));
 	    }
	    return ints;
	}
			
	var recording = false,
	currCallback;
	this.node.onaudioprocess = function(e){
	    var raw_audio = e.inputBuffer.getChannelData(0);
	    var audio_buf2 = new Int16Array(raw_audio.length)
	    
	    
	    function downsampleBuffer(buffer, sampleRate) {
		var rate = 16000
    		if (rate == sampleRate) {
    		    return buffer;
    		}
    		if (rate > sampleRate) {
    		    throw "downsampling rate show be smaller than original sample rate";
    		}
    		var sampleRateRatio = sampleRate / rate;
    		var newLength = Math.round(buffer.length / sampleRateRatio);
    		var result = new Float32Array(newLength);
    		var offsetResult = 0;
    		var offsetBuffer = 0;
    		while (offsetResult < result.length) {
    		    var nextOffsetBuffer = Math.round((offsetResult + 1) * sampleRateRatio);
    		    var accum = 0, count = 0;
    		    for (var i = offsetBuffer; i < nextOffsetBuffer && i < buffer.length; i++) {
    			accum += buffer[i];
    			count++;
    		    }
    		    result[offsetResult] = accum / count;
    		    offsetResult++;
    		    offsetBuffer = nextOffsetBuffer;
    		}
    		return result;
	    }


	    var downsampledBuffer = downsampleBuffer(raw_audio, this.context.sampleRate);

	    for (i=0; i<raw_audio.length; i++){
		var f = downsampledBuffer[i] * 32768
		if( f > 32767 ) f = 32767;
		if( f < -32768 ) f = -32768;
		audio_buf2[i] = swap16(f);
	    }

	    var ab = new ArrayBuffer(downsampledBuffer.length*Int16Array.BYTES_PER_ELEMENT);
	    var dv = new DataView(ab);
	    for (i=0; i<downsampledBuffer.length; i++){
		dv.setInt16(i*2,audio_buf2[i],false)
	    }

	    var audio_buf = new Uint8Array(ab, 0, downsampledBuffer.length*Int16Array.BYTES_PER_ELEMENT); 
	    //console.log(audio_buf)

	    var audio_pub_buf = [];
	    for (var i = 0; i < audio_buf.length; i++) {
		audio_pub_buf.push(audio_buf[i]);
	    } 
	    //console.log(audio_buf.length);
	    var audioToPub = new ROSLIB.Message({data: audio_pub_buf, sample_rate:this.context.sampleRate, num_channels:1, bitspersample:16});
	    audio_pub.publish(audioToPub);   
	    if (!recording) return;
	    worker.postMessage({
		command: 'record',
		buffer: [
		    e.inputBuffer.getChannelData(0),
		    e.inputBuffer.getChannelData(1)
		]
	    });
	}

	this.configure = function(cfg){
	    for (var prop in cfg){
		if (cfg.hasOwnProperty(prop)){
		    config[prop] = cfg[prop];
		}
	    }
	}

	this.record = function(){
	    recording = true;
	}

	this.stop = function(){
	    recording = false;
	}

	this.clear = function(){
	    worker.postMessage({ command: 'clear' });
	}

	this.getBuffers = function(cb) {
	    currCallback = cb || config.callback;
	    worker.postMessage({ command: 'getBuffers' })
	}

	this.exportWAV = function(cb, type){
	    currCallback = cb || config.callback;
	    type = type || config.type || 'audio/wav';
	    if (!currCallback) throw new Error('Callback not set');
	    worker.postMessage({
		command: 'exportWAV',
		type: type
	    });
	}

	this.exportMonoWAV = function(cb, type){
	    currCallback = cb || config.callback;
	    type = type || config.type || 'audio/wav';
	    if (!currCallback) throw new Error('Callback not set');
	    worker.postMessage({
		command: 'exportMonoWAV',
		type: type
	    });
	}

	worker.onmessage = function(e){
	    var blob = e.data;
	    currCallback(blob);
	}

	source.connect(this.node);
	this.node.connect(this.context.destination);   // if the script node is not connected to an output the "onaudioprocess" event is not triggered in chrome.
    };

    Recorder.setupDownload = function(blob, filename){
	var url = (window.URL || window.webkitURL).createObjectURL(blob);
	var link = document.getElementById("save");
	link.href = url;
	link.download = filename || 'output.wav';
    }

    window.Recorder = Recorder;

})(window);
