// Connecting to ROS
// -----------------
var ros_low_bw = new ROSLIB.Ros();

// If there is an error on the backend, an 'error' emit will be emitted.
ros_low_bw.on('error', function(error) {
  console.log(error);
});

// Find out exactly when we made a connection.
ros_low_bw.on('connection', function() {
  console.log('Connection made!');
});

// Create a connection to the rosbridge WebSocket server.
ros_low_bw.connect('ws://localhost:9090');

//Subscribing to a Topic
//----------------------
var listener_low_bw = new ROSLIB.Topic({
  ros : ros_low_bw,
  name : '/speech/hypothesis/best_bw_low',
  messageType : 'std_msgs/String'
});
var temp_str_low_bw ="";
listener_low_bw.subscribe(function(message) {	
  
  for(var c_low_bw=0; c_low_bw<message.data.length; c_low_bw++)
  {
    var elem_low_bw = document.getElementById("speech_low_bw");
    temp_str_low_bw+=message.data[c_low_bw];
    if(message.data[c_low_bw] == ' ')
    {	
	elem_low_bw.value = temp_str_low_bw;
	//console.log('Received message on ' + listener.name + ': '+ temp_str);
    }
    else if(message.data[c_low_bw] == '\n')
    {
          temp_str_low_bw = "";
    }
  }
});

