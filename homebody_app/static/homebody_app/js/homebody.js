// homebody.js
// Javascript code specific to the Homebody dialog system interface

//// Render Video Game State on GUI whenever we receive updates

var game_state_listener = new ROSLIB.Topic({
  ros : rbServer,
  name : '/game_state',
  messageType : 'std_msgs/String'
});


game_state_listener.subscribe(function(message) {
    render_video_game_state(message)
});

// transform coordinates so that objects are centered around x, y
// instead of having their corner sit at x, y
var transform_coords = function(x,y,w,h){
    var ans_x = (x - w/2)
    var ans_y = (y - h/2)
    return [ans_x, ans_y]
}

var render_video_game_state = function(game_state_message){
    // object center points and sizes arrive in game state 

    game_state = JSON.parse(game_state_message.data)
    console.log("game state: "+ JSON.stringify(game_state))

    user_width = game_state["user"]["width"]
    user_height = game_state["user"]["height"]
    user_position = transform_coords(game_state["user"]["current_user_x_percent"], game_state["user"]["current_user_y_percent"], user_width, user_height)    

    var user_html = '<div style="position:absolute;z:10;height:'+user_height+'%;width:'+user_width+'%;top:'+user_position[1]+'%;left:'+user_position[0]+'%"><img style="height:100%;width:100%" src='+static_root+'homebody_app/img/user.png></img></div>'

    var portals_html = ""
    for (i = 0; i < game_state["portals"].length; i++){
	portal = game_state["portals"][i]
	portal_width = portal["width"]
	portal_height = portal["height"]
	portal_position = transform_coords(portal["portal_x_percent"], portal["portal_y_percent"], portal_width, portal_height)
	console.log("rendering portal")
	portals_html += '<div style="position:absolute;z:9;height:'+portal_height+'%;width:'+portal_width+'%;top:'+portal_position[1]+'%;left:'+portal_position[0]+'%"><img style="height:100%;width:100%" src='+static_root+'homebody_app/img/portal.png></img></div>'
    }


    var objects_html = ""
    for (i = 0; i < game_state["objects"].length; i++){
	object = game_state["objects"][i]
	object_width = object["width"]
	object_height = object["height"]
	object_position = transform_coords(object["object_x_percent"], object["object_y_percent"], object_width, object_height)
	console.log("rendering object")
	objects_html += '<div style="position:absolute;z:9;height:'+object_height+'%;width:'+object_width+'%;top:'+object_position[1]+'%;left:'+object_position[0]+'%"><img style="height:100%;width:100%" src='+static_root+'homebody_app/img/'+object["name"]+'.png></img></div>'
    }

    change_bg_image(game_state["current_room"])
    document.getElementById("order_state").innerHTML = user_html + portals_html + objects_html
}

var change_bg_image = function(new_bg_image){
    $("#gui").css('background-image', 'url('+static_root+'homebody_app/img/'+new_bg_image+'.png)')
}


//// Send video game control messages

var game_control_pub = new ROSLIB.Topic({
    ros : rbServer,
    name : '/game_control',
    messageType : 'std_msgs/String'
});


// register keys to main.js keypress handler
keydownFunctionMap[37] = function(){
    var msg = new ROSLIB.Message({data: "left"})
    game_control_pub.publish(msg)   
}

keydownFunctionMap[39] = function(){
    var msg = new ROSLIB.Message({data: "right"})
    game_control_pub.publish(msg)   
}

keydownFunctionMap[38] = function(){
    var msg = new ROSLIB.Message({data: "up"})
    game_control_pub.publish(msg)   
}

keydownFunctionMap[40] = function(){
    var msg = new ROSLIB.Message({data: "down"})
    game_control_pub.publish(msg)   
}

