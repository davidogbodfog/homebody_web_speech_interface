from django.apps import AppConfig


class QsrDemoConfig(AppConfig):
    name = 'homebody_app'
